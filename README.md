# big_brothers_python_guide

## Big Brother's Study Guide for Introduction to Programming Using Python

An interdisciplinary project of students at
[Arlington Tech](https://careercenter.apsva.us/arlington-tech/) to satisfy two
design goals:

0. Create a study guide for the
   [MTA 98-381](https://www.microsoft.com/en-us/learning/exam-98-381.aspx)
   *Introduction to Programming Using Python* certification exam that will
   prepare Arlington Tech students to successfully earn this certification,
   fulling one of their graduation requirements.

0. Apply their creative writing skills to earn credit in their English 10
   Intensified class by theming the study guide around George Orwell's
   dsytopian novel,
   [Nineteen Eighty-Four](https://en.wikipedia.org/wiki/Nineteen_Eighty-Four).

### Measured Skills:

**Perform Operations using Data Types and Operators (20-25%)**

* Evaluate an expression to identify the data type Python will assign to each
  variable, including the following types:

  * `str`
  * `int`
  * `float`
  * `bool`
  * `list`

* Perform data and data type operations, including the following:

  * convert from one data type to another
  * construct data structures
  * perform indexing and slicing operations (`[]` and `[:]`)

* Determine the sequence of execution based on operator precedence with the
  following operators:

  * assignment (`=`)
  * comparison (`>`, `<`, `>=`, `<=`, `!=`)
  * logical (`and`, `or`, `not`)
  * arithmetic (`+`, `-`, `*`, `/`, `**`)
  * identity (`is`)
  * containment (`in`)


**Control Flow with Decisions and Loops (25-30%)**

* Construct and analyze code segments that use branching statements including
  the following:

  * `if`
  * `if ... else`
  * `if ... elif ... else`
  * nested conditional expressions
  * compound conditional expressions

* Construct and analyze code segments that perform iteration including the
  following statements:

  * `while`
  * `for`
  * `break`
  * `continue`
  * `pass`
  * nested loops
  * loops that include compound conditional expressions


**Perform Input and Output Operations (20-25%)**

* Construct and analyze code segments that perform file input and output
  operations including the following:

  * open
  * close
  * read
  * write
  * append
  * check existance
  * delete
  * `with` statement

* Construct and analyze code segments that perform console the following input
  and output operations:

  * read input from console
  * print formatted text
  * use command line arguments


**Document and Structure Code (15-20%)**

* Properly format and document code using the following:

  * indentation
  * white space
  * comments
  * documentation strings (aka *docstrings*) 
  * use `pydoc` to generate documentation

* Construct and anaylize code segments that include the following function
  defintion constructs:

  * `def` statement
  * `return` statement
  * parameters and arguments
  * call signatures
  * default values


**Perform Troubleshooting and Error Handling (5-10%)**

* Analyze, detect, and fix code segments that have the following types of
  errors:

  * syntax errors
  * logic errors
  * runtime errors

* Analyze and construct code segments that handle exceptions using the
  following:

  * `try`
  * `except`
  * `else`
  * `finally`
  * `raise`

**Perform Operations Using Modules and Tools (1-5%)**

* Peform basic operations using the following built-in modules:

  * `math`
  * `datetime`
  * `io`
  * `sys`
  * `os` and `os.path`
  * `random`

* Solve complex computing problems using the following built-in modules:

  * `math`
  * `datetime`
  * `random`
